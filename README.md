## Install ts-node globally
npm i -g ts-node

## Start
npm start

## Apis

PUT /user

POST /user/<user_name>/edit

GET /user/<user_name>

POST /subscription

## Deployed on Heroku
https://limitless-escarpment-62992.herokuapp.com/<PATH_OF_API>