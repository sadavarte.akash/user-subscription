import subscription from "./subscription/subscription"
import createUser from "./user/create-user"
import editUser from "./user/edit-user"
import getUser from "./user/get-user"

const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const port = process.env.PORT || 3000

//Extras
app.use(cors())
app.use(bodyParser.json())

//User Apis
app.use('/user', createUser)
app.use('/user', editUser)
app.use('/user', getUser)

//Subscription Api
app.use('/subscription',subscription)

//Start Express
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})