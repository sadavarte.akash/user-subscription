import express from 'express';
import { format_date } from '../common_functions';
import { subscriptions, users } from '../database/nedb';
let router = express.Router();
import DateExtension from '@joi/date';
import * as JoiImport from 'joi';
const Joi = JoiImport.extend(DateExtension);

const schema = Joi.object({
    user_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    rate: Joi.number()
        .required(),
    start_date: Joi.date().format('YYYY-MM-DD').required()
})

router.post('/', async (_req: any, res: any) => {
    try {
        let validation = schema.validate(_req.body)
        if (validation.error) {
            res.status(422).json(validation.error)
            return;
        }
        let user = await users.findOne({ user_name: _req.body.user_name })
        if (!user) {
            res.status(400).json({ status: 'FAILIURE', amount: (-1 * _req.body.rate), message: `No user with username ${_req.body.user_name}` })
        }
        if (user?.account_balance > _req.body.rate) {
            user.account_balance = user.account_balance - _req.body.rate;
            await users.update({ user_name: _req.body.user_name }, user);
            let response = await subscriptions.insert(_req.body)
            res.json({ status: 'SUCCESS', amount: -1 * parseFloat(_req.body.rate) })
        } else {
            res.status(400).json({ status: 'FAILIURE', amount: (-1 * _req.body.rate), message: `User with username ${_req.body.user_name} doesn't have enough balance` })
        }
    } catch (error) {
        console.log(error)
    }
});

export default router;