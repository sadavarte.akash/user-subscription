import express from 'express';
import { users } from '../database/nedb';
let router = express.Router();
const Joi = require('joi');

const schema = Joi.object({
    user_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required()
})

router.put('/', async (_req: any, res: any) => {
    try {
        let validation = schema.validate(_req.body)
        if (validation.error) {
            res.status(422).json(validation.error)
            return;
        }
        _req.body.created_at = new Date()
        _req.body.account_balance = 0
        let response = await users.insert(_req.body);
        res.json(response)
    } catch (error) {
        console.log(error)
    }
});

export default router;