import express from 'express';
import { users } from '../database/nedb';
let router = express.Router();
const Joi = require('joi');

const schema = Joi.object({
    user_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .optional(),
    account_balance: Joi.number()
        .optional()
})
router.post('/:user_name/edit', async (_req: any, res: any) => {
    try {
        let validation = schema.validate(_req.body)
        if (validation.error) {
            res.status(422).json(validation.error)
            return;
        }
        console.log('Put User was called')
        let user = await users.findOne({ user_name: _req.params.user_name });
        if (user) {
            let updated_object = { ...user, ..._req.body }
            let response = await users.update({ _id: user._id }, updated_object);
            res.json(updated_object);
        } else {
            res.status(400).json({ message: `No user with username ${_req.body.user_name}` })
        }
    } catch (error) {
        console.log(error)
    }
});

export default router;