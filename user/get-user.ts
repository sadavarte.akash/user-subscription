import express from 'express';
import { users } from '../database/nedb';
let router = express.Router();
const Joi = require('joi');

router.get('/:user_name', async (_req: any, res: any) => {
    try {
        let user = await users.findOne({ user_name: _req.params.user_name })
        if (!user) {
            res.status(400).json({ message: `No user with username ${_req.params.user_name}` })
        } else {
            res.json(user)
        }
    } catch (error) {

    }
});

export default router;